---
title: Oyun - Frequently Asked Questions
---

## What does X button mean?

If you have a question about a specific part of the Oyun user interface, you can
check out a complete description of the various parts of the UI in our [user
manual, available online.](/docs/) If the appropriate portion of the user manual
doesn't answer your question, see the bottom of the page for information about
how to contact the developer.

## Help! My question's not listed here!

If you've still got questions after checking the user manual and this FAQ, feel
free to get in touch with me, the developer.  I can be reached at
<charles@charlespence.net>, and I'll be happy to answer your question as soon as
I can.  I'll probably add it here, too, for the benefit of other users.
