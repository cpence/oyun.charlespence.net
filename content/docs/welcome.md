---
title: Welcome to Oyun
---

Oyun is a program you can use to run tournaments between various strategies
playing normal-form games such as the prisoner's dilemma.  The classic work on
such tournaments is Robert Axelrod's *The Evolution of Cooperation,* and Oyun
takes much inspiration from such tournaments.

Oyun allows you to create players, using a simple finite state machine syntax,
and these players then compete in tournaments -- either one-off, round-robin
tournaments or "evolutionary" tournaments in which strategies compete to
increase their proportion in a "population" over a number of generations.

For more information about Oyun, please visit the other sections of the user
manual, or the Oyun website at
[https://oyun.charlespence.net/](https://oyun.charlespence.net/).
