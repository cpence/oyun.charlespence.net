---
title: Installation
---

Oyun is available for almost any platform you can think of.  Read on to see how
to install it on your system.

## Installation on Windows

Oyun is distributed as a standard Windows installer.  Double-click it to
install, and then start Oyun from the shortcuts created in your Start Menu.

## Installation on Mac OS X

The installation of Oyun on OS X will be similar to most every application
you've installed on Mac OS X.  Download the OS X binary, which is a "disk image"
in .DMG format.  Open it, and you will find the Oyun application.  Drag the
application file to your "Applications" folder, and run it as usual.

## Installation on Linux

At the moment, we do not offer any binary packages for Oyun on Linux. However,
Linux installation can always be performed by compiling from source.  For
information about this, please consult the [technical documentation](/dev/).  In
short, a simple `cmake`, `make`, `sudo make install` should work.  If you want
to run the binary in a location other than the prefix under which it was
compiled, you will need to set the environment variable `DOCDIR` to the location
of the documentation file `oyun.htb`.

