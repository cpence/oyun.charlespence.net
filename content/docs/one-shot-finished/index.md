---
title: The One-Shot Tournament Finished Page
---

![Screensot](oneshotfinishpage.png)

This screen allows you to save the data from a finished One-Shot Tournament.
There are two varieties of data that may be saved.

## Save CSV...

Pressing this button will create a spreadsheet (in CSV format, which may be
opened by Microsoft Excel or the spreadsheet program of your choice) containing,
essentially, the results of the tournament as displayed on the previous screen.
This includes the full list of players and each player's total score, as well as
the list of matches, including each match's competitors, victor, and player
scores.

To save a CSV, press this button and select a location for the resulting file.

## Save Details...

Pressing this button will create a document (in RTF format, which may be opened
by Microsoft Word or the word processor of your choice) containing the full
details of the tournament which was just run: this includes all the data
mentioned above under the CSV file, as well as the full move list from every
game of every match.  It is unlikely that you will need this much detail, but if
you are attempting to debug problems with a given finite state machine, this
output may prove invaluable.

----------

That's it!  Once you have saved the data you wish to keep, press Finish to quit
Oyun.
