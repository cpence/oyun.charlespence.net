---
title: The Evolutionary Tournament Finished Page
---

![Screenshot](evofinishpage.png)

This screen allows you to save the data from a finished Evolutionary Tournament.
There are three varieties of data that may be saved.

## Save Image...

Pressing this button will save an image of the graph of the tournament results.
The image may be saved in JPG, PNG, or BMP format.  For saving
publication-quality images, it is recommended that you use the SVG option.

## Save SVG...

Pressing this button will save an image of the graph of the tournament, but in a
vector format (SVG) suitable for publication. This SVG file may be opened in
Adobe Illustrator, Inkscape, or any similar vector graphics editor.

## Save CSV...

Pressing this button will create a spreadsheet (in CSV format, which may be
opened by Microsoft Excel or the spreadsheet program of your choice) containing
the coordinates used to draw the graph on the preceding page.

----------

That's it!  Once you have saved the data you wish to keep, press Finish to quit
Oyun.

