---
title: Documentation Index
---

-   [Welcome to Oyun](/docs/welcome)
-   [Installation](/docs/installation)
-   [Release Notes](/docs/release-notes)
-   [Getting Started](/docs/getting-started)
-   Oyun Task Pages
    -   [Player Selection Page](/docs/players)
    -   [Tournament Type Page](/docs/tournament-type)
    -   [One-Shot Tournament Page](/docs/one-shot)
    -   [One-Shot Tournament Finished Page](/docs/one-shot-finished)
    -   [Evolutionary Tournament Page](/docs/evo-tournament)
    -   [Evolutionary Tournament Finished Page](/docs/evo-tournament-finished)
-   [Sample Players](/docs/sample-players)
-   [Glossary](/docs/glossary)

