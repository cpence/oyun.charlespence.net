---
title: The One-Shot Tournament Page
---

![Screenshot](oneshotpage.png)

On this page, you can run a One-Shot Tournament, which consists of a single run
in which each player plays against each other player in a round-robin
tournament.  The "winner" of the tournament is the player with the highest
score.

## Tournament Players List

On the left-hand side of the screen is a list of all the players that have been
added to the tournament.  To add more players, press the "Back" button to return
to the player selection screen.  The name of each player is listed, and after
the tournament has been run, each player's overall score will be shown.

## Tournament Maches List

In the center of the right-hand side of the screen is a list of all the matches
that will be played in this tournament.  Each match consists of five games, and
there is one match played for each pair of players in the players list.  After
the tournament is played, the victor of each match as well as the score
registered by players one and two (the first and second players listed under
"Competitors") will be listed.

To run the tournament, click the "Run Tournament" button above the tournament
matches list.  After the tournament is run, the winner will be displayed beneath
the maches list.  If you would like to see the detailed list of moves played
during a given match, click on a given match and press "Show Details...."  A
window will be displayed showing each move in each game.  You don't need to
record this detailed match information if you'd like to keep it; an opportunity
will be given to save this data on the next screen.

----------

After you have run the tournament, click the "Next" button to advance to the
next screen.
