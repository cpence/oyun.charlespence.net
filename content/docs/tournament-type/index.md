---
title: The Tournament Type Page
---

There are two types of tournament in Oyun, and this page lets you decide which
you would like to run.  You may choose either a One-Shot Tournament or an
Evolutionary Tournament.

## One-Shot Tournament

A one-shot tournament is a single run of a tournament in which each player plays
against each other player in a round-robin format.  The "winner" of the one-shot
tournament is the player with the highest total-sum score.  (For more
information, see the documentation for the One-Shot Tournament Page.)

## Evolutionary Tournament

An evolutionary tournament is a tournament in which a virtual "population" is
created, containing an equal amount of each sort of player.  The relative score
of each player as a fraction of the total in the population is calculated, and
these weights are used to determine the fractions in the next "generation." (For
more information, see the documentation for the Evolutionary Tournament Page.)

----------

When you have selected a tournament type, click the "Next" button to move to
that tournament's main page.

*Note:* If you would like to run both types of tournament with the same set of
players, you may do so -- simply run the first type of tournament, then click
the "Back" button until you return to the tournament type screen.  Select the
other type of tournament, and then click "Next".

