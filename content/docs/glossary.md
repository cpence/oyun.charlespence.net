---
title: Glossary
---

evolutionary tournament
:   An evolutionary tournament is a tournament in which a virtual "population"
    is created, containing an equal amount of each sort of player.  The relative
    score of each player as a fraction of the total in the population is
    calculated, and these weights are used to determine the fractions in the
    next "generation."

finite state machine
:   A finite state machine is a simple, minimal-programming way to write a
    strategy that can be used to play games in Oyun.

match
:   A match is a sequence of five games between two players.

one-shot tournament
:   A one-shot tournament is a single run of a tournament in which each player
    plays against each other player in a round-robin format.  The "winner" of
    the one-shot tournament is the player with the highest total-sum score.

prisoner's dilemma
:   The prisoner's dilemma is a simple 2x2 game in which the rational decision
    for both players is always to defect, while both could receive a higher
    payoff if they were to cooperate.
