---
title: Compiling Oyun
---

## Prerequisites

To compile Oyun, you need:

* Your platform's development tools
    * Windows: Visual C++, most likely any version after 6.0, including Visual C
      Express
    * Mac: Xcode (installed from your OS X DVDs, or from [[Apple's Xcode
      website|http://developer.apple.com/technologies/tools/xcode.html]]).  Note
      that recent versions of Xcode are available from the Mac App Store.
    * Linux: The standard GCC/development tools (Ubuntu: `sudo apt-get install
      build-essential`)
* [[CMake|http://www.cmake.org/]], at least v2.8
    * Windows: Download and install from the CMake website
    * Mac: Either download and install from CMake, or `brew install cmake`
    * Linux: Install through your distribution (Ubuntu: `sudo apt-get install
      cmake`; Gentoo: `sudo emerge cmake`)
* [[wxWidgets|http://www.wxwidgets.org/]], at least v3.0
    * Windows: Download the wxWidgets self extracting executable from the
      wxWidgets website, then build it.
    * Mac: Download the wxWidgets source package from the wxWidgets website, then build it.
        * Configure with the command
          ```
          configure --disable-dependency-tracking --disable-precomp-headers --with-cocoa --disable-shared --disable-compat24 \
            --enable-unicode --enable-macosx_arch="i386,x86_64"--with-libpng=builtin --with-libjpeg=builtin --with-zlib=builtin \
            --with-expat=builtin --with-libtiff=builtin
          ```
        * Note that you'll have a much easier time producing working wxWidgets
          binaries if you use the builtin libraries (zlib, libpng, libjpeg,
          expat) shipped with wxWidgets, as the command line above specifies.
    * Linux: Most linux distributions should be shipping wxWidgets 3.0. You may
      use either the GTK2 or GTK3 versions, though the developers have had
      better luck with GTK3.
* If you wish to build either the documentation or distribution packages, you
  will also need:
    * [[pandoc|http://johnmacfarlane.net/pandoc/]]
        * Windows: Install the self-extracting installer from pandoc.
        * Mac: `brew install haskell-platform`, then `cabal install pandoc`
          (make sure `~/.cabal/bin` is in your `PATH`)
        * Linux: Install through your distribution (Ubuntu: `sudo apt-get
          install pandoc`, Gentoo: `sudo emerge pandoc`)
    * A full TeX install, preferably [[TeX Live|http://tug.org/texlive/]]
        * Windows/Mac: Install the TeX Live distribution from its website
        * Linux: Install through your distribution (Ubuntu: `sudo apt-get
          install texlive`, Gentoo: `sudo emerge texlive`)
    * zip
        * Windows: Install [[Cygwin|http://www.cygwin.com/]], making sure to
          install the "zip" package.  Place the path to your Cygwin `bin`
          directory in your system PATH.
        * Mac: Already installed by default
        * Linux: Probably already installed through your distribution (Ubuntu:
          `sudo apt-get install zip`, Gentoo: `sudo emerge zip`)

##  Compiling

First, you need to unpack the Oyun source archive and create a build directory:

    tar xvf oyun-VERSION.tar.bz2
    mkdir build
    cd build

Now, run CMake to generate project/build files for your platform:

    cmake ../oyun-VERSION

On Windows, you will need to specify a generator:

    cmake -G "Visual Studio 2010" ../oyun-VERSION

An aside: if you wish to compile universal binaries on Mac (e.g., for
distribution), after the first run of CMake, open the CMakeCache.txt file and
change the value of `CMAKE_OSX_ARCHITECTURES` to `ppc;i386;x86_64`.  Also, for
maximum compatibility you'll want to set `CMAKE_OSX_DEPLOYMENT_TARGET` to `10.5`
and change `CMAKE_OSX_SYSROOT` to point to the 10.5 SDK.

If CMake runs successfully (and finds wxWidgets), you now just need to build
Oyun.  On Linux and Mac, simply execute `make`.  On Windows, open the generated
solution file (Oyun.sln or Oyun.dsw) in Visual Studio and compile.  The Oyun
binaries will be located in your build tree.  On Linux, you may simply execute
`make install` to install them.  On Windows and Mac, you probably want to run
`make package`, which will create an installer file (oyun-VERSION-win.exe or
oyun-VERSION-mac.dmg) which you can use to properly install Oyun.
